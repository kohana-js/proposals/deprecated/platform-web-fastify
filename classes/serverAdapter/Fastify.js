const { KohanaJS } = require('kohanajs');
const { RouteList } = require('@kohanajs/mod-route');
const RouteAdapter = require('../routeAdapter/Fastify');

class ServerAdapterFastify {
  static async setup() {
    const app = require('fastify')({
      logger: false,
      ignoreTrailingSlash: true,
    });

    app.register(require('fastify-formbody'));
    app.register(require('fastify-multipart'));

    if (KohanaJS.config.cookie) {
      app.register(require('fastify-cookie'), {
        secret: KohanaJS.config.cookie.salt,
        parseOptions: KohanaJS.config.cookie.options,
      });
    }

    await app.register(require('fastify-express'));

    if (KohanaJS.config.setup?.notFound) {
      app.setNotFoundHandler((request, reply) => {
        // Default not found handler with preValidation and preHandler hooks
        const { language } = request.params;
        const url = language ? `/${language}/pages/404` : '/pages/404';
        reply.redirect(`${url}?s=${encodeURIComponent(request.url)}`);
      });
    }

    RouteList.createRoute(app, RouteAdapter);

    return app;
  }
}

module.exports = ServerAdapterFastify;
